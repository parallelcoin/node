package leveldb

import (
	"testing"

	"gitlab.com/parallelcoin/node/goleveldb/leveldb/testutil"
)

func TestLevelDB(t *testing.T) {
	testutil.RunSuite(t, "LevelDB Suite")
}
